# Accedo React Homework ![CI status](https://img.shields.io/badge/build-passing-brightgreen.svg)

## Installation

#### `npm install` Installs node modules.

#### `npm run dev` Starts parcel boundler with hot reload at a free port. http://localhost:1234

### Full command list

```
"format": "prettier --write \"src/**/*.{js,jsx,css,scss,json} \"",
"format:check": "prettier --list-different \"src/**/*.{js,jsx,css,scss,json} \"",
"lint": "eslint \"src/*.{js,jsx}\"",
"lint:fix": "eslint --fix \"src/*.{js,jsx}\"",
"dev": "parcel src/index.html"
"build": "parcel build src/index.html"
```

### Build

`npm run build`
- Please find the compressed file in the repo. it should have been placed in the root directory to run properly.

### Dev environment

- Windows 10 64bit
- NodeJS v10.11.0
- NPM v6.4.1

### Dev tooling

- Eslint for linting
- Prettier for consistency
- Parcel for boundling

### Used libs

- [Flexbox grid](http://flexboxgrid.com/)

## License

[ISC](https://choosealicense.com/licenses/isc/)

# Homework description

### [WEB] Test Application (v2)

### Instructions

The goal of this work sample is to create a web app which lets you browse and watch a list of videos.

The application must use the simpleOvpApi (https://github.com/gergob/simpleOvpApi) for
loading data. The api is hosted on https://react-rent.herokuapp.com/.
Sample users for Login call:

### User Name - Password

- a - a
- abc@abc.com - abc@abc.com
- abc - abc
- test@test.com - test@test.com

### Requirements

- The app must work in Chrome 48+, Firefox 45+ and IE 10+
- The app must be developed using:
	- Frontend: React JS
	- Styling: CSS/SCSS/LESS
- The app must be usable by using the arrow keys (UP/DOWN/LEFT/RIGHT) for navigation, and the return key (ENTER) for selection and should also work using the mouse.
- The application should have a responsive design.
- The application must implement a Login form and should invoke the /login API - After a successful login, the application must display a list/grid of available movies/series:
	- Each element in the list/grid should display:
		- the image of the movie or series
		- the title of the movie or series
	- asset type (movie or series, this can be done using an icon or text)
	- Elements in the list/grid are selectable and should show a new layout once selected.
	- The new layout should display the following information from the API:
		- Movie/Series title
		- Movie/Series poster image
		- Description
		- ImDB rating
		- Genres
		- Length
		- Release Date
	- The new layout should have a `<video/>` tag where the trailer of the selected asset can be played back. The URL for the trailer is in the API response. There should be a PLAY/PAUSE button available on the page.

### Delivery

Your implementation must be delivered in a compressed archive containing all the necessary
files and resources (preferably Zip/7-Zip format). Also, an instruction on how to start and use the app is expected.
