import React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router } from "react-router-dom";

import Container from "./Components/Container";

const App = () => (
	<Router>
		<Container />
	</Router>
);

render(<App />, document.getElementById("root"));
