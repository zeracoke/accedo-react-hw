import React from "react";
import axios from "axios";

import Header from "./Header";
import Card from "./Card";

class List extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			// default list will be movies
			currList: "",
			remoteUrl: "https://react-rent.herokuapp.com/",
			userToken: "",
			currResponse: [],
			currClickedCard: ""
		};
		this.handleMovies = this.handleMovies.bind(this);
		this.handleSeries = this.handleSeries.bind(this);
		this.handleClickedCard = this.handleClickedCard.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);
	}
	componentDidMount() {
		// get userToken from location state and store it in current component state
		this.setState({ userToken: this.props.location.state.userToken });
		document.addEventListener("keydown", this.onKeyPress);
	}

	handleMovies() {
		// call axios after state is set
		this.setState({ currList: "movie" }, () => {
			this.axiosCall();
		});
		// and set actie card
		this.setState({
			currClickedCard: 1
		});
	}

	handleSeries() {
		// call axios after state is set
		this.setState({ currList: "serie" }, () => {
			this.axiosCall();
		});
		// and set active card
		this.setState({
			currClickedCard: 17
		});
	}

	axiosCall() {
		axios
			// we can update the state and call axios is state changes
			.get(this.state.remoteUrl + "api/" + this.state.currList, {
				headers: {
					// we use the token what we got from login component
					"X-SimpleOvpApi": this.state.userToken
				}
			})
			.then(response => {
				// store the response under "items"
				this.setState({ currResponse: response.data.items });
			});
	}

	handleClickedCard(key) {
		this.setState({ currClickedCard: key });
	}

	onKeyPress(event) {
		// listen to keypress vents
		if (event.key === "Enter") {
			this.setState({
				currClickedCard: ""
			});
		} else if (event.key === "ArrowLeft")
			this.setState(prevState => ({
				currClickedCard: prevState.currClickedCard - 1
			}));
		else if (event.key === "ArrowRight")
			this.setState(prevState => ({
				currClickedCard: prevState.currClickedCard + 1
			}));
		else if (event.key === "ArrowUp") {
			this.handleMovies(event);
			this.setState({
				currClickedCard: 1
			});
		} else if (event.key === "ArrowDown") {
			this.handleSeries(event);
			this.setState({
				currClickedCard: 17
			});
		}
	}

	render() {
		return (
			<div>
				<Header />
				<div className="wrap container-fluid">
					{/* triggering query */}
					<div className="row ">
						<button
							className="col-xs-offset-1 col-xs-5"
							onClick={this.handleMovies}
						>
							GET MOVIES ⭫
						</button>
						<button
							className="col-xs-offset-1 col-xs-5"
							onClick={this.handleSeries}
						>
							GET SERIES ⭭
						</button>
					</div>
				</div>
				<div className="wrap container-fluid">
					<div className="row">
						{/* getting all data from state and giving back states*/}
						{this.state.currResponse.map(list => {
							let classCard = "card";
							return (
								<Card
									list={list}
									handleClickedCard={this.handleClickedCard}
									classCard={classCard}
									active={this.state.currClickedCard}
									key={list.id}
									imageSrc={this.state.remoteUrl + list.imageSrc}
									cardType={this.state.currList}
								/>
							);
						})}
					</div>
				</div>
			</div>
		);
	}
}

export default List;
