import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";

import Login from "./Login";
import List from "./List";
import Warning from "./Warning";

function Container({ location }) {
	return (
		//container with routing
		<Switch location={location}>
			<Route exact path="/" component={Login} />
			<Route path="/list" component={List} />
			<Route path="/warning" component={Warning} />
		</Switch>
	);
}

export default withRouter(Container);
