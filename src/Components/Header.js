import React from "react";
import { Link } from "react-router-dom";

// header component for display routing
function Header() {
	return (
		<div className="top">
			<ul>
				<li>
					<Link to="/">Login</Link>
				</li>
				<li>
					<Link to="/list">List of Movies</Link>
				</li>
			</ul>
		</div>
	);
}

export default Header;
