import React from "react";

// stateless card componenet for grid
const Card = props => {
	// destructuring props
	const {
		handleClickedCard,
		onKeyPress,
		active,
		classCard,
		imageSrc,
		cardType,
		list
	} = props;
	return (
		<div className="col-md-3">
			<div
				role="menuitem"
				onKeyPress={() => onKeyPress(list.id)}
				onClick={() => handleClickedCard(list.id)}
				className={list.id === active ? "active " + classCard : classCard}
			>
				<h1>{list.title}</h1>
				<div className="imgcont">
					<img alt={list.title} src={imageSrc} />
					<h4 class="type">{cardType}</h4>
				</div>
				<h2>{list.genre}</h2>
				<div className="details">
					<div className="row">
						<video width="100%" height="127" controls poster={imageSrc}>
							<source src={list.videoSrc} type="video/mp4" />
							<track kind="captions" />
							Your browser does not support the video tag.
						</video>
						<h2>{list.description}</h2>
						<h2>{list.genre}</h2>
						<span>
							<b>Runtime:</b> {list.length}
						</span>
						<span>
							<b>Release date:</b> {list.releaseDate}
						</span>
						<span>
							<b>Actors:</b> {list.actors.join(", ")}
						</span>
						<span>
							<b>Director:</b> {list.director}
						</span>
						<span>
							<b>Rating:</b> {list.rating}
						</span>
						<span>{list.finished}</span>
						<span>
							<b>Price:</b> {list.price}
						</span>
						<span>
							<b>Get now for:</b> {list.promotionPrice}
						</span>
						<span>{list.isInPromotion}</span>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Card;
