import React from "react";
import axios from "axios";

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			userEmail: "",
			userPassword: "",
			userToken: "",
			error: ""
		};
		this.handleInputChange = this.handleInputChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
		this.onKeyPress = this.onKeyPress.bind(this);
	}

	handleInputChange(event) {
		// listening to input field changes and updating states based on that
		const target = event.target;
		const value = target.value;
		const name = target.name;
		this.setState({
			[name]: value
		});
	}

	componentDidMount() {
		document.addEventListener("keydown", this.onKeyPress);
	}

	handleSubmit(event) {
		// what happens when you press submit
		event.preventDefault();
		axios
			.post("https://react-rent.herokuapp.com/api/login", {
				// we send in the credentials provided before in the form
				email: this.state.userEmail,
				password: this.state.userPassword
			})
			.then(response => {
				// we set the value of userToken state in parent component
				// based on the header value we've got before
				this.setState({
					userToken: response.headers["x-simpleovpapi"]
				});
				// pushing usertoken into route - I don't understand passing
				// props between components clearly, but discovered this little trick
				this.props.history.push({
					pathname: "/list",
					state: { userToken: response.headers["x-simpleovpapi"] }
				});
			})
			.catch(error => {
				// let axios handle when the credentials are bad
				// render warning page
				this.props.history.push({
					pathname: "/warning",
					state: { error: error.response }
				});
			});
	}

	onKeyPress(event) {
		// listen to keypress vents
		if (event.key === "Enter") {
			if (
				this.state.userEmail == "" ||
				this.state.userPassword == "" ||
				(this.state.userEmail == "" && this.state.userPassword == "")
			) {
				alert("Please fill all fields. Thank you!");
			} else {
				this.handleSubmit();
			}
		} else if (event.key === "ArrowUp") {
			this.handleMovies(event);
			this.setState({
				currClickedCard: 1
			});
		} else if (event.key === "ArrowDown") {
			this.handleSeries(event);
			this.setState({
				currClickedCard: 17
			});
		}
	}

	render() {
		return (
			// form is set to change states based on input
			// on submit we trigger the handleSubmit event function
			<div className="wrap container-fluid">
				<form className="row" onSubmit={this.handleSubmit}>
					<input
						className="col-xs-offset-3 col-xs-6"
						type="text"
						name="userEmail"
						required
						placeholder="Enter your username (press ↹ - tab - to switch between fields)"
						value={this.state.userEmail}
						onChange={this.handleInputChange}
					/>
					<input
						className="col-xs-offset-3 col-xs-6"
						type="text"
						name="userPassword"
						required
						placeholder="Enter your password (press ↹ - tab - to switch between fields)"
						value={this.state.userPassword}
						onChange={this.handleInputChange}
					/>
					<input
						className="col-xs-offset-3 col-xs-6"
						type="submit"
						value="Submit (hit ⏎)"
					/>
				</form>
			</div>
		);
	}
}

export default Login;
